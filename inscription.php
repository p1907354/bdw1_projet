<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/utilisateur.php';

/* initialisation de la variable $stateMsg à "" */
$stateMsg = "";

/* si le bouton 'inscription' est saisie par l'utilisateur */
if (isset($_POST["valider"])) {

    /* on récupère les informations saisie par l'utilisateur dans les différents champs */
    $pseudo = $_POST["pseudo"];
    $_SESSION['pseudo'] = $pseudo;
    $hashMdp = md5($_POST["mdp"]);
    $hashConfirmMdp = md5($_POST["confirmMdp"]);

    /* connexion à la base de données */
    $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
    $_SESSION['link'] = $link;

    /* stocke la valeur de retour de la fonction 'checkAvailability()' */
    $available = checkAvailability($pseudo, $link);

    /* si les champs 'mot de passe' et 'confimation mot de passe' sont égaux */
    if ($hashMdp == $hashConfirmMdp) {

        /* si le pseudo est disponible */
        if ($available) {

            /* si la checkbox 'admin' est cochée */
            if ($_POST['admin'] == "on")

                /* on s'inscrit en tant qu'administrateur */
                registerAdmin($pseudo, $hashMdp, $link);
            else /* sinon on s'iscrit en tant qu'utilisateur */
                register($pseudo, $hashMdp, $link);

            /* on redirige l'utilisateur vers la page de connexion */
            header('Location: connexion.php?subscribe=yes');
        } else { /* sinon (pseudo non disponible) on affiche un message d'erreur */
            $stateMsg = '<div class="alert alert-danger"><b>Le pseudo demand&eacute; est d&eacute;j&agrave; utilis&eacute;</b></div>';
        }
    } else { /* sinon (mdp et confirmMdp différent) on affiche un message d'erreur */
        $stateMsg = '<div class="alert alert-danger"><b>Les champs mdp et confirmation du mdp doivent êtres identiques</b></div>';
    }
}

?>

<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Premi&egrave;re inscription</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="login-banner" style="margin-top: 250px;">
        <div class="form">
            <?php echo $stateMsg; ?>
            <form class="login-form" method="POST">
                <div class="form-floating mb-3">
                    <input type="text" name="pseudo" class="form-control" id="pseudo" placeholder="Pseudo">
                    <label for="pseudo">Pseudo</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" name="mdp" class="form-control" id="mdp" placeholder="Mot de passe">
                    <label for="mdp">Mot de passe</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" name="confirmMdp" class="form-control" id="confirmMdp" placeholder="Confirmation du mot de passe">
                    <label for="confirmMdp">Confirmation du mot de passe</label>
                </div>
                <div class="form-floating mb-3">
                    <div class="form-check">
                        <label for="admin" class="form-label">S'inscrire en tant qu'administrateur</label>
                        <input class="form-check-input" type="checkbox" name="admin" id="admin">
                    </div>
                </div>
                <button name="valider" class="btn btn-outline-success">Inscription</button>
                <p class="message">Déjà inscrit? <a href="connexion.php">Connectez-vous</a></p>
                <p class="message"><a href="index.php">Retour à la page d'accueil</a></p>
            </form>
        </div>
    </div>
</body>

</html>