<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/photo.php';
require_once 'fonctions/utilisateur.php';

/* initialisation de certaines variables à "" */
$user = "";
$statut = "";
$stateMsg = "";

/* connexion à la base de données */
$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
$_SESSION["link"] = $link;

/* si l'utilisateur est connecté */
if (isset($_SESSION['pseudo']) && utilisateurConnecte($_SESSION['pseudo'], $link)) {

    /* on récupère de pseudo */
    $pseudo = $_SESSION['pseudo'];

    /* on récupère le statut de l'utilisateur et on modifie la valeur des variable 
    $statut et $user précédement initialisé à "" */
    $result = getStatut($link, $pseudo);
    $statut = 'droits: ' . $result['statut'];
    $user = 'utilisateur: ' . $pseudo;

    /* si l'utilisateur appui sur le bouton "envoyer" */
    if (isset($_POST["valider"])) {

        /* on récupère le nom du fichier importé ainsi que son contenu et on déclare une variable 
        $file correspondant à l'emplacement du fichier */
        $nomFich = basename($_FILES["nomFich"]["name"]);
        $content_file = $_FILES['nomFich']['tmp_name'];
        
        $file = './data/DSC-' . $nomFich;

        /* on ulpoad le fichier sur le serveur dans le dossier data sous la forme 'DSC-nomdufichier.*' */
        move_uploaded_file($content_file, "./data/DSC-" . $nomFich);

        /* on stocke le type du fichier dans un variable $type_file */
        $type_file = mime_content_type($file);

        /* si aucun fichier n'a été importé on affiche un message d'erreur */
        if (!isset($_FILES['nomFich']))
            $stateMsg = "<div class='container-fluid alert alert-danger' role='alert'>Le fichier est manquant!</div>";

        /* si le type du fichier n'est pas jpeg/jpg/png/gif un message d'erreur est affiché */
        else if ($type_file !== "image/jpeg" && $type_file !== "image/png" && $type_file !== "image/gif")
            $stateMsg = "<div class='container-fluid alert alert-danger' role='alert'>Seul les fichiers de type .jpeg/.png/.gif sont acceptés!</div>";

        /* si la taille du fichier est supérieur à 800Kbytes soit 100Ko un message d'erreur est affiché */
        else if (filesize($file) > 800000)
            $stateMsg = "<div class='container-fluid alert alert-danger' role='alert'>Photo trop volumineuse (100ko max)!</div>";

        /* si le champs 'description' est vide un message d'erreur est affiché */
        else if (empty($_POST['description']))
            $stateMsg = "<div class='container-fluid alert alert-danger' role='alert'>La description est manquant!</div>";

        /* si la catégorie sélectionné est égale à la valeur 'Sélectionner la catégorie' cela signifie 
        qu'aucune catégorie n'a été sélectionné donc un message d'erreur s'affiche */
        else if ($_POST['categorie'] == "Sélectionner la catégorie")
            $stateMsg = "<div class='container-fluid alert alert-danger' role='alert'>Aucune catégorie n'a été sélectionnée!</div>";

        /* Sinon */
        else {

            /* on stocke le nom du fichier dans $nomFich */
            $nomFich = 'DSC-' . basename($_FILES['nomFich']['name']);

            /* on stocke la description dans $description */
            $description = $_POST['description'];

            /* on stocke la catégorie dans $categorie puis on le passe en paramètre de la fonction 'returnIdCategorie()' 
            qui renvoie une chaine de caractère correspondant à l'id de la categorie */
            $categorie = $_POST['categorie'];
            $catId = returnIdCategorie($categorie);

            /* on ajoute la photo avec ses informations dans la base de donnée */
            ajoutPhoto($catId, $nomFich, $description, $link, $pseudo);

            /* on récupère l'id de la photo dans la base de donnée */
            $result = recupIdPhoto($link, $nomFich);

            /* on la convertie en entier et on appel la fonction 'header' qui renvoie l'utilisateur à l'url passé en paramètre */
            $id = intval($result['photoId']);
            $url = 'photoInfos.php?id=' . $id;
            header('Location: ' . $url . '');
        }
    }
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
    <title>Ajout d'une photo</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light shadow p-3 mb-5 bg-body rounded">
        <div class="mx-auto order-0">
            <h1>Mini Pinterest</h1>
        </div>
        <div class="navbar-collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="index.php?categorie=tout&valider=">
                        <input class="btn btn-sm btn-outline-secondary" type="button" value="retour">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid col-md-6 bg-light shadow p-3 mb-5 bg-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <p class="text-left"><b><?php echo $user; ?></b></p>
                </div>
                <div class="col-md-6" style="margin-left: 135px;">
                    <p class="text-right"><b><?php echo $statut; ?></b></p>
                </div>
            </div>
        </div>
        <h1>Ajout d'une photo</h1>
        <?php echo $stateMsg; ?>
        <form enctype="multipart/form-data" method="POST">
            <div class="mb-3">
                <label for="photo" class="form-label"><b>Importer une photo</b></label>
                <input type="file" name="nomFich" class="form-control" id="photo">
            </div>
            <div class="mb-3">
                <label for="description" class="form-label"><b>Saisir une description de cette photo: </b></label>
                <textarea class="form-control" id="description" name="description" rows="3" placeholder="saisissez la descritpion de l'image"></textarea>
            </div>
            <div class="mb-3">
                <label for="formFile" class="form-label"><b>Choisir la catégorie à laquelle apartient cette photo: </b></label>
                <select name="categorie" class="form-select" aria-label="Default select example">
                    <option default>Sélectionner la catégorie</option>
                    <option value="Animaux">Animaux</option>
                    <option value="Paysage">Paysage</option>
                    <option value="Voiture">Voiture</option>
                    <option value="Monument">Monument</option>
                    <option value="Cuisine">Cuisine</option>
                    <option value="Mangas et Animés">Mangas et Animés</option>
                    <option value="Jeux vidéo">Jeux vidéo</option>
                    <option value="Films et Séries">Films et Séries</option>
                </select>
            </div>
            <button name="valider" class="btn btn-outline-success">Envoyer</button>
        </form>
    </div>
</body>

</html>