<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/photo.php';
require_once 'fonctions/utilisateur.php';

/* initialisation de plusieurs variables à "" */
$time = "";
$img = "";
$statut = "";
$titre = "";
$msg = "";
$statistiqueButton = "";
$optionMesPhotos = "";
$user = "";

/* une variable $button est initialisé avec les champs html correspondant aux bouton de connexion et d'inscription */
$button = '<a href="connexion.php"><button type="button" class="btn btn-outline-success me-2">Connexion</button></a>
           <a href="inscription.php"><button type="button" class="btn btn-sm btn-outline-dark">Inscription</button></a>';

/* fonction qui met à jour un champs <option> à 'selected' si la valeur passé en paramètre correspond */ 
$selected = function ($value) {
    echo (isset($_GET['valider']) && $_GET['categorie'] == $value) ? "selected" : '';
};

/* connexion à la base de données */
$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
$_SESSION["link"] = $link;

/* si le bouton 'valider' est saisie par l'utilisateur */
if (isset($_GET["valider"])) {

    /* la categorie sélectionnée est stockée dans $categorie */
    $categorie = $_GET["categorie"];

    /* si le pseudo de la session n'existe pas (aucun utilisateur de connecté) */
    if (isset($_SESSION['pseudo']) == NULL)
        
        /* on appel la fonction 'tabQueryDisconnected()' */
        $tab_results = tabQueryPhotoDisconnected($link, $categorie);

    /* sinon si l'utilisateur est connecté */
    else if (isset($_SESSION['pseudo']) && utilisateurConnecte($_SESSION['pseudo'], $link)) {
        /* ajout d'une catégorie 'mes photos' correspondant aux photos que l'utilisateur a ajouté */
        $optionMesPhotos = '<option value="Mes photos">Mes photos</option>';

        /* récupération de pseudo/statut de l'utilisateur pour les afficher */
        $pseudo = $_SESSION['pseudo'];
        $result = getStatut($link, $pseudo);
        $statut = 'droits: ' . $result['statut'];
        $user = 'utilisateur: ' . $pseudo;

        /* si le statut de l'utilisateur est égale à 'administrateur' */
        if ($result['statut'] == "administrateur") 

            /* on ajoute un bouton qui renvoie à la page de statistique du site */
            $statistiqueButton = '<a href="statistiques.php"><button type="button" class="btn btn-outline-success me-3">Statistiques</button></a>';
        
        /* appel à la fonction 'tabQueryPhotoUser()' qui récupère les photos de la catégorie entrée en paramètre */
        $tab_results = tabQueryPhotoForUser($link, $pseudo, $categorie);

        /* les boutons 'ajout' et 'deconnexion' remplace les boutons 'connexion' et 'inscription' */
        $button = '<a href="ajout.php"><button type="button" class="btn btn-outline-success me-2">Ajout</button></a>'
            . $statistiqueButton .
            '<button name="deconnexion" class="btn btn-sm btn-outline-danger">Deconnexion</button>';

        /* si l'utilisateur appuie sur le bouton 'deconnexion' */
        if (isset($_POST["deconnexion"])) {
            $link = $_SESSION['link'];

            /* on remet la valeur de la variable de session contenant le pseudo de l'utilisateur à 'null' */
            $_SESSION['pseudo'] = null;

            /* l'etat de l'utilisateur est update à 'disconnected' */
            setDisconnected($pseudo, $link);

            /* on ferme la connexion à la base de donnée */
            closeConnexion($link);

            /* on renvoie l'utilisateur à la page d'accueil */
            header('Location: index.php');
        }
    }

    /* gestion de l'affichage du titre dynamiquement en fonction de la catégorie sélectionné ainsi que le message qui dit combien 
    de photos on été sélectionnées */
    $titre = titreAffichage($categorie);
    $nb = count($tab_results);
    $msg =  '<div class="container-fluid col-md-6 alert alert-success" role="alert">' . $nb . " photo(s) selectionnée(s)</div>";

    /* on stocke dans la variable $img les champs html correspondant aux images à afficher */
    foreach ($tab_results as $value) {
        $img .= '<div class="fond_image"><a href="photoInfos.php?id=' . $value['photoId'] . '"><input type="image" src="./data/' . $value['nomFich'] . '"
        alt="' . $value['description'] . '" title="' . $value['description'] . '" width="200px" height="150px" border="2"></a></div>' . "\n";
    }
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
    <title>Catalogue Photos</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light shadow p-3 mb-5 bg-body rounded">
        <div class="mx-auto order-0">
            <h1>Mini Pinterest</h1>
        </div>
        <div class="navbar-collapse w-100 order-3 dual-collapse2">
            <form method="POST">
                <?php echo $button; ?>
            </form>
        </div>
    </nav>
    <p id="time"></p>
    </br>
    <?php echo $msg; ?>
    <div class="row">
        <div class="container-fluid col-md-6 bg-light shadow p-3 mb-5 bg-body">
            <div class="container-fluid col-md-12">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="text-left"><b><?php echo $user; ?></b></p>
                        </div>
                        <div class="col-md-6" style="margin-left: 135px;">
                            <p class="text-right"><b><?php echo $statut; ?></b></p>
                        </div>
                    </div>
                </div>
                <form method="GET">
                    <label>Quelles photos souhaitez-vous afficher ?</label>
                    <select name="categorie" class="w-50">
                        <option value="tout" <?php $selected("tout") ?>>Toutes les photos</option>
                        <?php echo $optionMesPhotos; ?>
                        <option value="Animaux" <?php $selected("Animaux") ?>>Animaux</option>
                        <option value="Paysage" <?php $selected("Paysage") ?>>Paysage</option>
                        <option value="Voiture" <?php $selected("Voiture") ?>>voiture</option>
                        <option value="Monument" <?php $selected("Monument") ?>>Monument</option>
                        <option value="Cuisine" <?php $selected("Cuisine") ?>>Cuisine</option>
                        <option value="Mangas et Animés" <?php $selected("Mangas et Animés") ?>>Mangas et Animés</option>
                        <option value="Jeux vidéo" <?php $selected("Jeux vidéo") ?>>Jeux vidéo</option>
                        <option value="Films et Séries" <?php $selected("Films et Séries") ?>>Films et Séries</option>
                    </select>
                    <button name="valider" class="btn btn-outline-success" style="float: right;">Valider</button>
                </form>
            </div>
            <div class="container-fluid col-md-12">
                <h1 id="titre-affichage"><?php echo $titre; ?></h1>
                <?php echo $img; ?>
            </div>
        </div>
    </div>
</body>

</html>