<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/utilisateur.php';

/* initialisation de certaine variable de type string à "" */
$stateMsg = "";
$successMsg = "";

/* si l'utilisateur appuie sur 'connexion' */
if (isset($_POST["valider"])) {

  /* le pseudo est stocké dans la variable $pseudo */
  $pseudo = $_POST["pseudo"];

  /* la valeur de la variable de session avec l'index 'pseudo' est mis à jour avec le nouveau pseudo */
  $_SESSION['pseudo'] = $pseudo;

  /* le mot de passe est hasher et stocké dans $hashMdp */
  $hashMdp = md5($_POST["mdp"]);

  /* connexion à la base de données */
  $link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
  $_SESSION['link'] = $link;

  /* $exist récupère la valeur de retour de la fonction getUser qui renvoie true si le pseudo de 
  l'utilisateur est valide, false sinon */
  $exist = getUser($pseudo, $hashMdp, $link);

  /* si l'utilisateur existe */
  if ($exist) {
    /* on met à jour l'etat de l'utilisateur à 'connected */
    setConnected($pseudo, $link);

    /* on set le bon fuseau horaire et on stocke la date ainsi que l'heure de conexion de l'utilisateur dans la base de données */
    date_default_timezone_set('Europe/Paris');
    $date = date('d-m-y G:i:s');
    dateUpdate($link, $date, $pseudo);

    /* on appel la fonction 'header qui renvoie l'utilisateur à l'url indiqué en paramètre */
    header("Location: index.php?categorie=tout&valider=");
  } else { /* sinon on affiche un message d'erreur */
    $stateMsg = '<div class="alert alert-danger"><b>Le couple pseudo/mot de passe ne correspond &agrave; aucun utilisateur enregistr&eacute;</b></div>';
  }
}

/* si l'url contient la valeur 'subscribe' alors on affiche un message */
if (isset($_GET["subscribe"])) {
  $successMsg = '<div class="alert alert-success"><b>L\'inscription a bien &eacute;t&eacute; effectu&eacute;e, vous pouvez vous connecter</b></div>';
}
?>

<!doctype html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <title>Bienvenue sur le Chat de BDW1</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <div class="login-banner" style="margin-top: 250px;">
    <div class="form">
      <?php echo $stateMsg; ?>
      <?php echo $successMsg; ?>
      <form method="POST">
        <div class="form-floating mb-3">
          <input type="text" name="pseudo" class="form-control" id="pseudo" placeholder="Pseudo">
          <label for="pseudo">Pseudo</label>
        </div>
        <div class="form-floating mb-3">
          <input type="password" name="mdp" class="form-control" id="mdp" placeholder="Mot de passe">
          <label for="mdp">Mot de passe</label>
        </div>
        <button name="valider" class="btn btn-outline-success">Connexion</button>
        <p class="message">Pas encore inscrit? <a href="inscription.php">Inscrivez-vous</a></p>
        <p class="message"><a href="index.php">Retour à la page d'accueil</a></p>
      </form>
    </div>
  </div>
</body>

</html>