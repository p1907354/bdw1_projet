<?php
session_start();
require_once 'fonctions/utilisateur.php';
require_once 'fonctions/statistiques.php';
require_once 'fonctions/bd.php';

/* initialisation de ces variables à "" */
$nbPhotoCategoriesRow = "";
$nbPhotoUsersRow = "";
$lastConnectionUsers = "";

/* connexion à la base de donnée */
$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
$_SESSION["link"] = $link;

/* récupération le pseudo et le statut de l'utilisateur */
$pseudo = $_SESSION['pseudo'];
$result = getStatut($link, $pseudo);
$statut = 'droits: ' . $result['statut'];
$user = 'utilisateur: ' . $pseudo;

/* on stocke le résultat des différentes fonctions dans les variables suivantes */
$nbUser = nombreUtilisateur($link);
$nbPhotoUsers = nbPhotoUtilisateurs($link);
$nbPhotoCategories = nbPhotoCategories($link);
$lastConnection = lastConnectionUsers($link);

/* on parcours chaque tableau et on construit les tableaux contenants les informations */
foreach ($nbPhotoUsers as $value) {
    $nbPhotoUsersRow .= "<tr><td>" . $value['pseudo'] . "</td><td>" . $value['nbPhotos'] . "</td></tr>";
}

foreach ($lastConnection as $value) {
    $lastConnectionUsers .= "<tr><td>" . $value['pseudo'] . "</td><td>" . $value['dateLastConnection'] . "</td></tr>";
}

foreach ($nbPhotoCategories as $value) {
    $nbPhotoCategoriesRow .= "<tr><td>" . $value['nomCat'] . "</td><td>" . $value['nbPhotos'] . "</td></tr>";
}

/* on stocke l'html de chaque tableau avec les informations souhaitées; */
$tableNbUser = "<div class='container-fluid col-md-12'>
                    <table class='table table-striped'>
                    <tr><th class='table-dark'>Nombre d'utilisateurs inscrits</th></tr>
                    <tr><td>" . $nbUser[0]['nbUtilisateurs'] . "</td></tr>
                </div>";

$tableNbPhotoUsers = "<div class='container-fluid col-md-12'>
                        <table class='table table-striped'>
                        <tr><th class='table-dark'>Utilisateur</th><th class='table-dark'>Nombre de photos</th></tr>"
    . $nbPhotoUsersRow .
    "</div>";

$tableNbPhotoCategories = "<div class='container-fluid col-md-12'>
                            <table class='table table-striped'>
                            <tr><th class='table-dark'>Catégorie</th><th class='table-dark'>Nombre de photos</th></tr>"
    . $nbPhotoCategoriesRow .
    "</div>";

$tableaDateLastConnection = "<div class='container-fluid col-md-12'>
                                <table class='table table-striped'>
                                <tr><th class='table-dark'>Utilisateur</th><th class='table-dark'>Dernière connexion au site</th></tr>"
                                . $lastConnectionUsers .
                            "</div>";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
    <title>Statistiques</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light shadow p-3 mb-5 bg-body rounded">
        <div class="mx-auto order-0">
            <h1>Mini Pinterest</h1>
        </div>
        <div class="navbar-collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="index.php?categorie=tout&valider=">
                        <input class="btn btn-sm btn-outline-secondary" type="button" value="retour">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid col-md-6 bg-light shadow p-3 mb-5 bg-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <p class="text-left"><b><?php echo $user; ?></b></p>
                </div>
                <div class="col-md-6" style="margin-left: 135px;">
                    <p class="text-right"><b><?php echo $statut; ?></b></p>
                </div>
            </div>
        </div>
        <h1>Statistiques du site</h1>
        <br>
        <?php echo $tableNbUser; ?>
        <?php echo $tableNbPhotoUsers; ?>
        <?php echo $tableNbPhotoCategories; ?>
        <?php echo $tableaDateLastConnection; ?>
    </div>
</body>

</html>