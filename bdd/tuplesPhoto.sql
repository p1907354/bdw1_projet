INSERT INTO Categorie (nomCat) VALUES ('Animaux');
INSERT INTO Categorie (nomCat) VALUES ('Paysage'); 
INSERT INTO Categorie (nomCat) VALUES ('Voiture');
INSERT INTO Categorie (nomCat) VALUES ('Monument');
INSERT INTO Categorie (nomCat) VALUES ('Cuisine');
INSERT INTO Categorie (nomCat) VALUES ('Mangas et Animés');
INSERT INTO Categorie (nomCat) VALUES ('Jeux vidéo');
INSERT INTO Categorie (nomCat) VALUES ('Films et Séries');

INSERT INTO Utilisateur (pseudo, mdp, statut, etat, dateLastConnection) VALUES ('p1907354', '01c7babbcec8ed2f08d3e2e1436399e4', 'administrateur', 'disconnected', '');

INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (1, "animaux1.jpg", "p1907354", "Zèbre dans la savane", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (1, "animaux2.jpg", "p1907354", "Antilope dans la savane", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (1, "animaux3.jpg", "p1907354", "Puma dans la savane", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (1, "animaux4.jpg", "p1907354", "Lion dans la savane", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (1, "animaux5.jpg", "p1907354", "marmotte", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (2, "paysage1.jpg", "p1907354", "montagne", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (2, "paysage2.jpg", "p1907354", "mer", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (2, "paysage3.jpg", "p1907354", "plage", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (2, "paysage4.jpg", "p1907354", "couché de soleil", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (3, "voiture1.jpg", "p1907354", "voiture dans les montagnes", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (3, "voiture2.jpg", "p1907354", "voiture de sport", "show");
INSERT INTO Photo (catId, nomFich,	pseudo, description, etat) VALUES (3, "voiture3.jpg", "p1907354", "voiture en ville", "show");


