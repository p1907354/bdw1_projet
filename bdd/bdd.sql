CREATE TABLE IF NOT EXISTS `Categorie` (
    `catId` integer NOT NULL AUTO_INCREMENT,
    `nomCat` varchar(250) NOT NULL,
    PRIMARY KEY (`catId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Utilisateur` (
    pseudo varchar(250) NOT NULL,
    mdp varchar(250) NOT NULL,
    etat varchar(250) NOT NULL,
    statut varchar(250) NOT NULL,
    dateLastConnection varchar(250),
    PRIMARY KEY (`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `Photo` (
    photoId integer NOT NULL AUTO_INCREMENT,
    catId integer NOT NULL,
    pseudo varchar(250) NOT NULL,
    nomFich varchar(250) NOT NULL,
    description varchar(250) NOT NULL,
    etat varchar(250) NOT NULL,
    FOREIGN KEY (`catId`) REFERENCES Categorie(`catId`),
    FOREIGN KEY (`pseudo`) REFERENCES Utilisateur(`pseudo`),
    PRIMARY KEY (`photoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
