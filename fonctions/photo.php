<?php

/* cette fonction prend en entrée le pseudo de l'utilisateur et une categorie et renvoie un tableau contenant les informations 
sur les photos correspondant à la catégorie passé en paramètre. On appel cette fonction lorsque l'utilisateur est connecté */
function tabQueryPhotoForUser($link, $pseudo, $categorie)
{
    if ($categorie == 'tout')
        $query = "SELECT * FROM Photo WHERE pseudo = '" . $pseudo . "' OR etat = 'show';";
    else if ($categorie == "Mes photos") 
        $query = "SELECT * FROM Photo WHERE pseudo = '". $pseudo ."';";
    else
        $query = "SELECT p.* FROM Photo p JOIN Categorie c ON p.catId = c.catId WHERE p.pseudo = '" . $pseudo . "' AND c.nomCat = '" . $categorie . "' OR p.etat = 'show' AND c.nomCat = '" . $categorie . "';";
    $result =  executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

/* cette fonction prend en entrée une categorie et renvoie un tableau contenant les informations sur les images correspondants 
à la catégorie passé en paramètre. On appel cette fonction lorsque l'utilisateur n'est pas connecté */
function tabQueryPhotoDisconnected($link, $categorie)
{
    if ($categorie == 'tout')
        $query = "SELECT p.* FROM Photo p JOIN Utilisateur u ON p.pseudo = u.pseudo WHERE u.statut = 'administrateur' AND p.etat = 'show';";
    else
        $query = "SELECT p.* FROM Utilisateur u JOIN Photo p ON u.pseudo = p.pseudo JOIN Categorie c ON p.catId = c.catId WHERE u.statut = 'administrateur' AND c.nomCat = '" . $categorie . "' AND p.etat = 'show';";
    $result =  executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

/* cette fonction prend en entrée un id de catégorie, un nom de fichier, une description et un pseudo. Elle insert une photo avec 
les informations correspondantes dans la relation Photo */
function ajoutPhoto($catId, $nomFich, $description, $link, $pseudo)
{
    $query = 'INSERT INTO Photo (catId, nomFich, pseudo, description, etat) VALUES (' . $catId . ' , "' . $nomFich . '", "' . $pseudo . '", "' . $description . '", "show");';
    executeUpdate($link, $query);
}

/* cette fonction prend en entrée un nom de fichier et renvoie l'id de la photo stocké dans la relation 'Photo' correspondante */
function recupIdPhoto($link, $nomFich)
{
    $query = "SELECT photoId FROM Photo WHERE nomFich = '" . $nomFich . "';";
    $result = executeQuery($link, $query);
    return mysqli_fetch_assoc($result);
}

/* cette fonction prend en entrée un id et update la valeur de l'attribue 'etat' de la relation 'Photo' correspondant de 'show' à 'hide' */
function hidePhoto($link, $id)
{
    $query = "UPDATE Photo SET etat = 'hide' WHERE photoId = $id;";
    executeUpdate($link, $query);
}

/* cette fonction prend en entrée un id et update la valeur de l'attribue 'etat' de la relation 'Photo' correspondant de 'hide' à 'show' */
function showPhoto($link, $id)
{
    $query = "UPDATE Photo SET etat = 'show' WHERE photoId = $id;";
    executeUpdate($link, $query);
}

/* supprime la photo dans la relation 'Photo' correspondant à l'id passé en paramètre */
function deletePhoto($link, $id)
{
    $query = "DELETE FROM Photo WHERE photoId = $id;";
    executeUpdate($link, $query);
}

/* cette fonction prend en entrée un id, une description et un id de catégorie et modifie les attribues 'description' et 'catId' de la relation 
'Photo' correspondant à l'id passé en paramètre */
function modifPhoto($link, $id, $description, $catId)
{
    if ($catId == NULL)
        $query = "UPDATE Photo SET description = '" . $description . "' WHERE photoId = $id;";
    else if (empty($description))
        $query = "UPDATE Photo SET catId = $catId WHERE photoId = $id;";
    else
        $query = "UPDATE Photo SET description = '" . $description . "', catId = $catId WHERE photoId = $id;";
    executeUpdate($link, $query);
}

/* cette fonction prend en entrée un nom de catégorie et affiche le titre correspondant */
function titreAffichage($nomCat)
{
    if ($nomCat == 'tout')
        return '<h1>Toutes les photos</h1>';
    else
        return '<h1>Photos de la catégorie ' . $nomCat . ' </h1>';
}

/* Cette fonction prend en entée un id et affiche les informations correspondantes */
function tableauInfos($id, $link)
{
    $query = "SELECT p.nomFich, p.description, c.nomCat, p.pseudo, p.etat FROM Photo p JOIN Categorie c ON p.catId = c.catId WHERE p.photoId = " . $id . ";";
    $result = executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

/* cette fonction prend en entrée une catégorie et renvoie une chaine de caractère correspondant à l'id de la catégorie */
function returnIdCategorie($categorie)
{
    if ($categorie == 'Animaux')
        return 1;
    else if ($categorie == 'Paysage')
        return 2;
    else if ($categorie == 'Voiture')
        return 3;
    else if ($categorie == 'Monument')
        return 4;
    else if ($categorie == 'Cuisine')
        return 5;
    else if ($categorie == 'Mangas et Animés')
        return 6;
    else if ($categorie == 'Jeux vidéo')
        return 7;
    else if ($categorie == 'Films et Séries')
        return 8;
}
