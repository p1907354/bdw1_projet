<?php

/*Cette fonction prend en entrée un pseudo à ajouter à la relation utilisateur et une connexion et 
retourne vrai si le pseudo est disponible (pas d'occurence dans les données existantes), faux sinon*/
function checkAvailability($pseudo, $link)
{
	$query = "SELECT pseudo FROM Utilisateur WHERE pseudo = '" . $pseudo . "';";
	$result = executeQuery($link, $query);
	return mysqli_num_rows($result) == 0;
}

/* Cette fonction prend en entrée un pseudo et un mot de passe et enregistre le nouvel utilisateur dans la relation Utilisateur via la connexion*/
function register($pseudo, $hashPwd, $link)
{
	$query = "INSERT INTO Utilisateur VALUES ('" . $pseudo . "', '" . $hashPwd . "', 'disconnected', 'utilisateur', '');";
	executeUpdate($link, $query);
}

/* Cette fonction prend en entrée un pseudo et un mot de passe et enregistre le nouvel administrateur dans la relation Utilisateur via la connexion */
function registerAdmin($pseudo, $hashPwd, $link)
{
	$query = "INSERT INTO Utilisateur VALUES ('" . $pseudo . "', '" . $hashPwd . "', 'disconnected', 'administrateur', '');";
	executeUpdate($link, $query);
}

/*Cette fonction prend en entrée un pseudo d'utilisateur et change son état en 'connected' dans la relation 
Utilisateur via la connexion*/
function setConnected($pseudo, $link)
{
	$query = "UPDATE Utilisateur SET etat= 'connected' WHERE pseudo = '" . $pseudo . "';";
	executeUpdate($link, $query);
}

/*Cette fonction prend en entrée un pseudo et mot de passe et renvoie vrai si l'utilisateur existe (au moins un tuple dans le résultat), faux sinon*/
function getUser($pseudo, $hashPwd, $link)
{
	$query = "SELECT pseudo FROM Utilisateur WHERE pseudo = '" . $pseudo . "' AND mdp = '" . $hashPwd . "' AND etat = 'disconnected';";
	$result = executeQuery($link, $query);
	return (mysqli_num_rows($result) == 1);
}

/* cette fonction prend en entrée un pseudo et renvoie la valeur correspondant à l'attribue statut de la relation Utilisateur */
function getStatut($link, $pseudo)
{
	$query = "SELECT statut FROM Utilisateur WHERE pseudo = '" . $pseudo . "';";
	$result = executeQuery($link, $query);
	return mysqli_fetch_assoc($result);
}

/*Cette fonction renvoie un tableau (array) contenant tous les pseudos d'utilisateurs dont l'état est 'connected'*/
function getConnectedUsers($link)
{
	$query = "SELECT pseudo FROM Utilisateur WHERE etat = ' connected';";
	$result = executeQuery($link, $query);
	return mysqli_fetch_row($result);
}

/*Cette fonction prend en entrée un pseudo d'utilisateur et change son état en 'disconnected' dans la relation 
utilisateur via la connexion*/
function setDisconnected($pseudo, $link)
{
	$query = "UPDATE Utilisateur SET etat= 'disconnected' WHERE pseudo = '" . $pseudo . "';";
	executeUpdate($link, $query);
}

function dateUpdate($link, $date, $pseudo) {
	$query = "UPDATE Utilisateur SET dateLastConnection = '". $date ."' WHERE pseudo = '". $pseudo ."';";
	executeUpdate($link, $query);
}


/* cette fonction prend en entrée un pseudo et renvoie vrai si l'utilisateur avec le pseudo passé en paramètre est connecté */
function utilisateurConnecte($pseudo, $link)
{
    $query = "SELECT etat FROM Utilisateur WHERE pseudo = '$pseudo' AND etat = 'connected';";
    $result = executeQuery($link, $query);
    return (mysqli_num_rows($result) == 1);
}