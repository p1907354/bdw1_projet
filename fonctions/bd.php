<?php
$dbHost = "localhost";
$dbUser = "p1907354";
$dbPwd = "Salsa37Gizmo";
$dbName = "p1907354";

/*Cette fonction prend en entrée l'identifiant de la machine hôte de la base de données, les identifiants (login, mot de passe) d'un utilisateur autorisé 
sur la base de données contenant les tables pour les photos et les catégories et renvoie une connexion active sur cette base de donnée. Sinon, un message 
d'erreur est affiché.*/
function getConnection($dbHost, $dbUser, $dbPwd, $dbName)
{
	$link = mysqli_connect($dbHost, $dbUser, $dbPwd, $dbName);
	if (!$link)
		echo "Echec lors de la connexion à la base de donnees: ("
			. mysqli_connect_errno() . ") " . mysqli_connect_error();
	return $link;
}

/*Cette fonction prend en entrée une connexion vers la base de données ainsi 
qu'une requête SQL SELECT et renvoie les résultats de la requête. Si le résultat est faux, un message d'erreur est affiché*/
function executeQuery($link, $query)
{
	$result = mysqli_query($link, $query);
	if (!$result) {
		echo "La requete n'a pas pu être executé";
	}
	return $result;
}

/*Cette fonction prend en entrée une connexion vers la base de données ainsi 
qu'une requête SQL INSERT/UPDATE/DELETE et ne renvoie rien si la mise à jour a fonctionné, sinon un 
message d'erreur est affiché.*/
function executeUpdate($link, $query)
{
	$result = mysqli_query($link, $query);
	if (!$result) {
		echo "La requete n'a pas pu être executé";
	}
}

/*Cette fonction ferme la connexion active $link passée en entrée*/
function closeConnexion($link)
{
	mysqli_close($link);
}
