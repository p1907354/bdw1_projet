<?php

/* fonction qui renvoie un tableau contenant le nombre d'utilisateur inscrits */
function nombreUtilisateur($link)
{
    $query = "SELECT COUNT(*) AS nbUtilisateurs FROM Utilisateur;";
    $result = executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

/* fonction qui renvoie un tableau contenant le nombre de photo par utilisateur */
function nbPhotoUtilisateurs($link)
{
    $query = "SELECT u.pseudo, COUNT(p.nomFich) AS nbPhotos FROM Photo p JOIN Utilisateur u ON u.pseudo = p.pseudo GROUP BY u.pseudo;";
    $result = executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

/* fonction qui renvoie un tableau contenant le nombre de photo par catégorie */
function nbPhotoCategories($link)
{
    $query = "SELECT c.nomCat, COUNT(p.catId) AS nbPhotos FROM Categorie c JOIN Photo p ON p.catId = c.catId GROUP BY c.nomCat;";
    $result = executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

/* cette fonction renvoie un tableau contenant les pseudos des utilisateurs avec leur date de dernière connexion au site */
function lastConnectionUsers($link) {
    $query = "SELECT pseudo, dateLastConnection FROM Utilisateur GROUP BY pseudo;";
    $result = executeQuery($link, $query);
    $tab = array();
    while ($row = mysqli_fetch_assoc($result)) {
        array_push($tab, $row);
    }
    return $tab;
}

?>