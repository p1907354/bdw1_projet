<?php
session_start();
require_once 'fonctions/bd.php';
require_once 'fonctions/photo.php';
require_once 'fonctions/utilisateur.php';

/* on initialise les variables suivantes à "" */
$user = "";
$statut = "";
$button = "";
$backButton = "";

/* connexion à la base de données */
$link = getConnection($dbHost, $dbUser, $dbPwd, $dbName);
$_SESSION["link"] = $link;

/* on récupère l'id dans l'url et on appel la fonction 'tableauInfos' qui renvoie les informations 
correspondant à la photo d'id $id */
$id = $_GET["id"];
$tab = tableauInfos($id, $link);

/* on entre les informations 'description' et 'categorie' dans un tableau */
$src =  './data/' . $tab[0]['nomFich'] . '';
$categorieRow = "<a href=\"index.php?categorie=" . $tab[0]['nomCat'] . "&valider=\">" . $tab[0]['nomCat'] . "</a>";
$descriptionRow = $tab[0]['description'];

/* si l'etat de la photo est à 'hide' on affiche le bouton 'Afficher' */
if ($tab[0]['etat'] == 'hide')
    $affichage = '<input type="submit" name="show" class="btn btn-outline-secondary" value="Afficher">';

/* sinon on affiche le bouton 'Cacher' */
else $affichage = '<input type="submit" name="hide" class="btn btn-outline-secondary" value="Cacher">';

/* on affiche les boutons 'Supprimer' et 'Modifier' */
$deleteButton = '<input type="submit" name="delete" class="btn btn-outline-danger" value="Supprimer">';
$modifButton = '<input type="submit" name="modif" class="btn btn-outline-success" value="Modifier">';

/* si l'utilisateur appuie sur le bouton 'Modifier' */
if (isset($_POST['modif'])) {
    /* on remplace les champs 'description' et 'catégorie' du tableau */
    $descriptionRow = '<textarea class="form-control" id="description"  name="description" rows="3" placeholder="saisissez la descritpion de l\'image"></textarea>';
    $categorieRow = '
                        <select name="categorie"  class="form-select" aria-label="Default select example">
                            <option default>Sélectionner la catégorie</option>
                            <option value="Animaux">Animaux</option>
                            <option value="Paysage">Paysage</option>
                            <option value="Voiture">Voiture</option>
                            <option value="Monument">Monument</option>
                            <option value="Cuisine">Cuisine</option>
                            <option value="Mangas et Animés">Mangas et Animés</option>
                            <option value="Jeux vidéo">Jeux vidéo</option>
                            <option value="Films et Séries">Films et Séries</option>
                        </select>';

    /* le bouton 'Modifier' est remplacé par le bouton 'Valider' */
    $modifButton = '<input type="submit" name="valider" class="btn btn-outline-success" value="Valider">';
    $affichage = "";

    /* le bouton 'Cacher/Afficher' est remplacé par le bouton 'Retour' */
    $backButton = '<input type="submit" name="back" class="btn btn-outline-secondary" value="Retour">';
    $deleteButton = "";
}

/* si le bouton 'Valider' est saisie par l'utilisateur */
if (isset($_POST['valider'])) {

    /* récupération des valeurs entrées dans les différents champs */
    $description = $_POST['description'];
    $catId = returnIdCategorie($_POST['categorie']);

    /* appel à la fonction 'modifPhoto()' qui met à jour les valeurs dans la base de données */
    modifPhoto($link, $id, $description, $catId);

    /* renvoie l'utilisateur sur la page d'info de la photo */
    header('Location: photoInfos.php?id=' . $id . '');
}

/* si l'utilisateur appuie sur le bouton 'Retour' */
if (isset($_POST['back'])) {

    /* l'utilisateur est renvoyé sur la page d'information de la photo */
    header('Location: photoInfos.php?id=' . $id . '');
}

/* déclaration de la variable $infos qui affiche l'image et le tableau d'informations */
$infos = "<div class='container-fluid col-md-12'><img src=" . $src . " align='left' width='400' height='300' border='2' class='img-fluid'></div>
        <div class='container-fluid col-md-12'>
            <table class='table table-striped'>
            <tr><th class='table-dark'>Description</th><td>" . $descriptionRow . "</td></tr>
            <tr><th class='table-dark'>Nom du fichier</th><td>" . $tab[0]['nomFich'] . "</td></tr>
            <tr><th class='table-dark'>Ajouté par</th><td>" . $tab[0]['pseudo'] . "</td></tr>
            <tr><th class='table-dark'>Catégorie</th><td>" . $categorieRow . "</td></tr>
        </div>";

/* si le bouton 'Cacher' est appuyé */
if (isset($_POST['hide'])) {

    /* mise à jour de l'etat de la photo de 'show' à 'hide' et le bouton 'Cacher' est remplacé par le bouton 'Afficher' */
    hidePhoto($link, $id);
    $affichage = '<input type="submit" name="show" class="btn btn-outline-secondary" value="Afficher">';
}

/* si le bouton 'Afficher' est appuyé */
if (isset($_POST['show'])) {
    /* mise à jour de l'etat de la photo de 'hide' à 'show' et le bouton 'Afficher' est remplacé par le bouton 'Cacher' */
    showPhoto($link, $id);
    $affichage = '<input type="submit" name="hide" class="btn btn-outline-secondary" value="Cacher">';
}

/* si le bouton 'Supprimer' est appuyé */
if (isset($_POST['delete'])) {
    /* la photo est supprimé de la base et l'utilisateur est redirigé vers la page d'accueil */
    deletePhoto($link, $id);
    header('Location: index.php?categorie=tout&valider=');
}

/* Si l'utilisateur est connecté */
if (isset($_SESSION['pseudo']) && utilisateurConnecte($_SESSION['pseudo'], $link)) {

    /* on récuppère le pseudo ainsi que le statut */
    $pseudo = $_SESSION['pseudo'];
    $result = getStatut($link, $pseudo);
    $statut = 'droits: ' . $result['statut'];
    $user = 'utilisateur: ' . $pseudo;

    /* si la photo affiché est celle ajouté par l'utilisateur on affiche les différents boutons de modif/suppression/cacher etc.. */
    if ($tab[0]['pseudo'] == $pseudo) {
        $button = $modifButton . $backButton . $affichage . $deleteButton;
    } else if ($result['statut'] == 'administrateur') { /* sinon on affiche pas le bouton pour cacher/afficher la photo */
        $button = $modifButton . $backButton . $deleteButton;
    }
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css">
    <title>Information sur la photo</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light shadow p-3 mb-5 bg-body rounded">
        <div class="mx-auto order-0">
            <h1>Mini Pinterest</h1>
        </div>
        <div class="navbar-collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="index.php?categorie=tout&valider=">
                        <input class="btn btn-sm btn-outline-secondary" type="button" value="retour">
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid col-md-6 bg-light shadow p-3 mb-5 bg-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <p class="text-left"><b><?php echo $user; ?></b></p>
                </div>
                <div class="col-md-6" style="margin-left: 135px;">
                    <p class="text-right"><b><?php echo $statut; ?></b></p>
                </div>
            </div>
        </div>
        <h1>Les détails sur cette photo</h1>
        </br>
        <form method="POST">
            <?php echo $infos; ?>
            <div class="btn-group info-button" role="group" aria-label="Basic outlined example">
                <?php echo $button; ?>
            </div>
        </form>
    </div>
</body>

</html>